<?php
/*
 *  Author: Oskars Andersons
 *  URL: base.lv | @balssieraksti
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('balssieraksti', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function balssieraksti_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load Balss ieraksti scripts (header.php)
function balssieraksti_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!
    }
}

// Load Balls ieraksti custom script footer.php)
wp_enqueue_script( 'balssierakstiscripts', get_template_directory_uri() . '/js/scripts.js', array(), false, true );


// Load Balss ieraksti styles
function balssieraksti_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('balssieraksti', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('balssieraksti'); // Enqueue it!
}

// Register Balss ieraksti Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'balssieraksti'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'balssieraksti'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'balssieraksti') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => 'Widget Area 1',
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => 'Widget Area 2',
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
         'name' => 'Shop sidebar',
         'id' => 'shop-sidebar',
         'before_widget' => '<aside id="%1$s" class="widget group %2$s">',
         'after_widget' => '</aside>',
         'before_title' => '<span class="widget-title">',
         'after_title' => '</span>',
         ));
         register_sidebar(array(
         'name' => 'Footer #1',
         'id' => 'footer-1',
         'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
         'after_title'   => '</h2>',
         'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
         'after_widget'  => '</div></div>',
         ));
         register_sidebar(array(
         'name' => 'Footer #2',
         'id' => 'footer-2',
         'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
         'after_title'   => '</h2>',
         'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
         'after_widget'  => '</div></div>',
         ));
         register_sidebar(array(
         'name' => 'Footer #3',
         'id' => 'footer-3',
         'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
         'after_title'   => '</h2>',
         'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
         'after_widget'  => '</div></div>',
         ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'balssieraksti_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_enqueue_scripts', 'balssieraksti_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add Balss ieraksti Menu
add_action('init', 'html5wp_pagination'); // Add Balss ieraksti Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'balssierakstigravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

/*------------------------------------*\
	Woocommerce and stuff
\*------------------------------------*/

// Add Woocommerce support
function balsis_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'balsis_add_woocommerce_support' );

// Remove shop li.product card link
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 ); 
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

// Add sidebar before Woocommerce shop 
add_action('woocommerce_before_shop_loop', 'sidebar_wrapper_open',10);
add_action('woocommerce_before_shop_loop', 'balsis_show_sidebar', 20);
add_action('woocommerce_before_shop_loop', 'sidebar_wrapper_close',30);
function sidebar_wrapper_open() {
	echo '<div class="woo-shop-sidebar-wrapper">';
}
function balsis_show_sidebar() {
	return dynamic_sidebar( 'shop-sidebar' );
}
function sidebar_wrapper_close() {
	echo '</div>';
}	

// Other minor woocommerce changes 
add_filter( 'woocommerce_show_page_title', '__return_null' );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 90 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

// change Woocommerce Add to cart button text
add_filter( 'woocommerce_product_add_to_cart_text', function( $text ) {
	global $product;
	if ( $product ) {
		$text = __( 'Rezervēt', 'woocommerce' );
	}
	return $text;
}, 10 );

// Change return to shop text
add_filter( 'gettext', 'change_woocommerce_return_to_shop_text', 20, 3 );
function change_woocommerce_return_to_shop_text( $translated_text, $text, $domain ) {
       switch ( $translated_text ) {
                      case 'Atgriezties uz veikalu' :
   $translated_text = __( 'Rezervēt balsis', 'woocommerce' );
   break;
  }
 return $translated_text; 
}

// Jānotestē, ko šis dara

add_filter( 'yith_wcan_unfiltered_args', 'yith_wcan_unfiltered_args' );

if( ! function_exists( 'yith_wcan_unfiltered_args' ) ){
    function yith_wcan_unfiltered_args( $unfiltered_args ){
        if( ! empty( $_GET['s'] ) ){
            if( isset( $unfiltered_args['s'] ) ){
                unset($unfiltered_args['s']);
            }
        }
        return $unfiltered_args;
    }
}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 ); 

add_action( 'woocommerce_before_single_product_summary', 'show_add_to_favorites');
function show_add_to_favorites(){
    echo '<div class="row single-nav">';
    echo '<a href="'.get_site_url().'"><img src="'.get_stylesheet_directory_uri().'/img/arrow-right.svg" />Atpakaļ</a>';
    echo do_shortcode('[yith_wcwl_add_to_wishlist]');
    echo '</div>';
}

add_action( 'woocommerce_single_product_summary', 'order_this_voice_block');
function order_this_voice_block(){
    echo '<h2>Rezervēt šo balsi</h2>';
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_product_thumbnails', 'woocommerce_template_single_title',5);

add_action( 'woocommerce_shop_loop_item_title', 'show_single_player', 5 );
add_action( 'woocommerce_product_thumbnails', 'show_single_player',10);
function show_single_player(){
    if( have_rows('valoda') ):
       $number_of_cols = count( get_field( 'valoda' ) );
        $i = 0;
        $isFirst = true;
        echo '<div class="audio-wrapper">';
        // echo '<progress class="time-progress" value="0" max="1"></progress>';
        if ( $number_of_cols > 1 ) { }
             echo '<div class="tab">';
            //generate tabs
            while ( have_rows('valoda') ) : the_row(); ?>
                <button class="tablinks <?php if ($isFirst) {echo 'active';}  ?>" onclick="openTab(event, '<?php echo get_sub_field('lang'); ?>')"><?php echo get_sub_field('lang'); ?></button>
            <?php 
            $isFirst = false;
            endwhile;
            echo '</div>';
        
        echo '<div class="audioring"></div>';
        echo '<div class="player-buttons">';
        // generate tab content
        while ( have_rows('valoda') ) : the_row(); ?>
            <div id="<?php echo get_sub_field('lang'); ?>" class="tabcontent content-<?php echo $i; ?>">
               <?php if ( get_sub_field('story') ) { ?>
                    <button onclick="playSound(this)" class="mybtn story-<?php echo $i; ?>"><i class="fa fa-play"></i><span>Stāsts</span></button>
                    <audio ontimeupdate="updateProgress(this)" class="audio-file" style="display: none;" controlsList="nodownload">
                            <source src="<?php echo get_sub_field('story'); ?>" type="audio/mpeg">
                        Your browser does not support the audio element.
                    </audio>
                    
                <?php } ?>
                <?php if ( get_sub_field('advert') ) { ?>
                    <button onclick="playSound(this)" class="mybtn advert-<?php echo $i; ?>"><i class="fa fa-play"></i><span>Reklāma</span></button>
                    <audio ontimeupdate="updateProgress(this)" class="audio-file" style="display: none;" controlsList="nodownload">
                            <source src="<?php echo get_sub_field('advert'); ?>" type="audio/mpeg">
                        Your browser does not support the audio element.
                    </audio>
                <?php } ?>
                <?php if ( get_sub_field('story') ) { ?>
                    <button onclick="playSound(this)" class="mybtn decribe-<?php echo $i; ?>"><i class="fa fa-play"></i><span>Apraksts</span></button>
                    <audio ontimeupdate="updateProgress(this)" class="audio-file" style="display: none;" controlsList="nodownload">
                            <source src="<?php echo get_sub_field('describe'); ?>" type="audio/mpeg">
                        Your browser does not support the audio element.
                    </audio>
                <?php } ?>
            </div>
            <?php $i++;
        endwhile;
        echo '</div></div>';
        endif;          
}

// Change Related products text
function custom_related_products_text( $translated_text, $text, $domain ) {
  switch ( $translated_text ) {
    case 'Saistītie Produkti' :
      $translated_text = __( 'Noklausies arī', 'woocommerce' );
      break;
  }
  return $translated_text;
}
add_filter( 'gettext', 'custom_related_products_text', 20, 3 );


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_product_thumbnails', 'woocommerce_template_single_excerpt', 20 );

remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );


function page_wrapper_start(){
    echo '<div class="wrapper">';
}
function page_wrapper_ends(){
    echo '</div>';
}

add_action('woocommerce_before_single_product_summary', 'page_wrapper_start',5);

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_action('woocommerce_after_single_product_summary', 'page_wrapper_ends',45); //close woo wrapper
add_action( 'woocommerce_after_single_product_summary', function(){ echo '<div class="related-bg">'; }, 50 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 55 );
add_action( 'woocommerce_after_single_product_summary', function(){ echo '</div>'; }, 60 );

add_action( 'woocommerce_before_shop_loop', 'page_wrapper_start',5 );
add_action( 'woocommerce_after_shop_loop', 'page_wrapper_ends', 90);

add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );

add_shortcode ('woo_cart_but', 'woo_cart_but' );
/**
 * Create Shortcode for WooCommerce Cart Menu Item
 */
function woo_cart_but() {
	ob_start();
 
        $cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
        $cart_url = wc_get_cart_url();  // Set Cart URL
  
        ?>
        <li><a class="menu-item nav-cart cart-contents" href="<?php echo $cart_url; ?>" title="My Basket">
	    <?php
        if ( $cart_count > 0 ) {
       ?>
            <span class="cart-contents-count"><?php echo $cart_count; ?></span>
        <?php
        }
        ?>
		</a></li>
        <?php
	        
    return ob_get_clean();
 
}
// add Cart and Account icons to Main menu.
add_filter('wp_nav_menu_items', 'add_admin_link', 10, 2);
function add_admin_link($items, $args){
	
		$cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
        $cart_url = wc_get_cart_url();  // Set Cart URL
    if( $args->theme_location == 'primary' ){
        $items .= '<li><a class="nav-account" href="'. wc_get_page_permalink( 'myaccount' ) .'"></a></li>';
		$items .= '<li><a class="nav-cart" href="'. $cart_url .'"><span class="cart-contents-count">'. $cart_count .'</span></a></li>';
    }
	
    return $items;
}

add_filter('posts_where', 'posts_where');
function posts_where($where){
		if(is_search())
		{
			$s = $_GET['s'];

			if(!empty($s))
			{
				if(is_numeric($s))
				{
					global $wpdb;

					$where = str_replace('(' . $wpdb->posts . '.post_title LIKE', '(' . $wpdb->posts . '.ID = ' . $s . ') OR (' . $wpdb->posts . '.post_title LIKE', $where);
				}
				elseif(preg_match("/^(\d+)(,\s*\d+)*\$/", $s)) // string of post IDs
				{
					global $wpdb;

					$where = str_replace('(' . $wpdb->posts . '.post_title LIKE', '(' . $wpdb->posts . '.ID in (' . $s . ')) OR (' . $wpdb->posts . '.post_title LIKE', $where);
				}
			}
		}

		return $where;
	}

// Custom field in checkout
add_filter( 'woocommerce_checkout_fields' , 'woocommerce_checkout_field_editor' );
// Our hooked in function - $fields is passed via the filter!
function woocommerce_checkout_field_editor( $fields ) {
    $fields['billing']['billing_project_name'] = array(
        'label'     => __('Projekta nosaukums', 'woocommerce'),
        'placeholder'   => _x('Balss ierakstu projekts #', 'placeholder', 'woocommerce'),
        'required'  => true
    );
    $fields['billing']['billing_regno'] = array(
        'label'     => __('Pers.kods/PVN reģ. nr.', 'woocommerce'),
        'placeholder'   => _x('Pers.kods/PVN reģ. nr.', 'placeholder', 'woocommerce'),
        'required'  => false
    );
    return $fields;
}

// Show custom field in admin post status
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'edit_woocommerce_checkout_page', 10, 1 );
function edit_woocommerce_checkout_page($order){
    global $post_id;
    $order = new WC_Order( $post_id );
    echo '<p><strong>'.__('Projekta nosaukums').':</strong> ' . get_post_meta($order->get_id(), '_billing_project_name', true ) . '</p>';
}

// Wishlist sharelink
function wpb_hook_javascript() {
  if (is_page ('21')) { 
    ?>
        <script type="text/javascript">
         window.onload = function(){
			var shareLink = document.getElementById('yith-wcwl-form').getAttribute('action');
			document.getElementById('shareUrl').value = shareLink;
		};
        </script>
    <?php
  }
}
add_action('wp_footer', 'wpb_hook_javascript');

// Shop random order. View settings drop down order by Woocommerce > Settings > Products > Display
// add_filter( 'woocommerce_get_catalog_ordering_args', 'custom_woocommerce_get_catalog_ordering_args' );
function custom_woocommerce_get_catalog_ordering_args( $args ) {
    $orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
    if ( 'random_list' == $orderby_value ) {
        $args['orderby'] = 'rand';
        $args['order'] = '';
        $args['meta_key'] = '';
    }
    return $args;
}
// add_filter( 'woocommerce_default_catalog_orderby_options', 'custom_woocommerce_catalog_orderby' );
add_filter( 'woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby' );
function custom_woocommerce_catalog_orderby( $sortby ) {
    $sortby['random_list'] = 'Random';
    return $sortby;
}

/** * @desc Remove in all product type */
function woo_remove_all_quantity_fields( $return, $product ) {
  return true;
}
add_filter( 'woocommerce_is_sold_individually', 'woo_remove_all_quantity_fields', 10, 2 );

// Price from
function balssieraksti_format_price_range( $price, $from, $to ) {
    return sprintf( '%s %s', __( 'Sākot no', 'balssieraksti' ), wc_price( $from ) );
}
add_filter( 'woocommerce_format_price_range', 'balssieraksti_format_price_range', 10, 3 );

/**
 * Trim zeros in price decimals
 **/
 add_filter( 'woocommerce_price_trim_zeros', '__return_true' );

add_filter ( 'wc_add_to_cart_message', 'wc_add_to_cart_message_filter', 10, 2 );
function wc_add_to_cart_message_filter($message, $product_id = null) {
    return;
}
?>
