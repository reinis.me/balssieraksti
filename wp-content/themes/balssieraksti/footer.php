<!--</div> 
 /wrapper -->	
			<!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="wrapper">
					<?php if ( is_front_page() ){ ?>
						<div id="subscribe-block">
							<div class="col-6">
								<h2>Pierakstīties jaunumiem</h2>
								<p>Esi pirmais, kas uzzina par jaunajiem balss aktieriem un citiem labumiem</p>
							</div>
							<div class="col-6">
							<?php echo do_shortcode('[contact-form-7 id="2034" title="Mailchimp"]'); ?>
							</div>
						</div>
					<?php } ?>
					<div class="row justify-between align-center">
					<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
						<div class="row">
							<img class="footer-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer-logo.svg" alt="logo-dark"/>
							<div>
					        <?php dynamic_sidebar( 'footer-1' ); ?>
					    	</div>
					    </div>
					<?php endif; ?>
					<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<div class="row">
					        <?php dynamic_sidebar( 'footer-2' ); ?>
					    </div>
					<?php endif; ?>
					<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
						<div class="row">
					        <?php dynamic_sidebar( 'footer-3' ); ?>
					    </div>
					<?php endif; ?>
					</div> <!-- row -->
				</div> <!-- wrapper -->
			</footer>
			<!-- /footer -->

		<?php wp_footer(); ?>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	</body>
</html>
