<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'balsis_lv' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'P?p}l@|V|RyNJ/d:~:;Fj[-[,@sl,R$@<8swW?4S*nv7/+xYrE{kB&g-KB(fAhk)' );
define( 'SECURE_AUTH_KEY',  '^!FqyhyC+|5XSf;7rx!)b?-LzWEXR0CK^Vg`:FRA47vgYC6946Rk=/0_6$k2*d~/' );
define( 'LOGGED_IN_KEY',    'XUDNGb>JtW1<l<LF7MTCo9HjuhTeYuUCAia0<.IkwQbkx)LJ&PVZ81f%u)v&2VoR' );
define( 'NONCE_KEY',        'S`r.MOV]]mCey*Qe])A-tM0aK]z.m TBk/:%``LizTg#c^XX0aC+n,I#Ir^4Fs%0' );
define( 'AUTH_SALT',        'odMvO/?})V6)@4ZVLC1;UM+v%W&zfE;P`d:1*/q_w_13UL3EosHOEHV.v</1%-XB' );
define( 'SECURE_AUTH_SALT', 'XrQ#D>|vjm=XFlY[nEN&0%L(#l>!}hfz=l:Xj~MNO3a;ltZ~*GgF:jKt]^b!HvU[' );
define( 'LOGGED_IN_SALT',   'AtZ8P+T1t; s9<1-pO}z^J4yQW?V=`eD_kmWT&EJDdQ;DMho1R=j,O9l+cK~)h!*' );
define( 'NONCE_SALT',       '9do7vr[@+|W&H2.u1U!.6!|Q!wkM_RHl(,_z:%|{(#/mv/1BU-77XY8MH-jW#FA}' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
